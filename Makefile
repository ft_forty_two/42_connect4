# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fkalb <fkalb@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/03/10 17:55:02 by fkalb             #+#    #+#              #
#    Updated: 2014/03/10 19:17:45 by kgilonne         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc
NAME = connect4
LIBS_DIR=./lib/
FLAGS = -W -Wall -Wextra -Werror -O1 -pedantic

INCS = $(addprefix -I, $(addsuffix /includes/, $(INC_DIRS)))
INC_DIRS = .\
		   ./libft

#LIBS = $(addprefix ./lib/, $(addsuffix .a, $(LIB_LIST)))
#LIB_LIST = libft

DEPEND=$(addprefix $(LIBS_DIR), $(addsuffix .a, $(DEPEND_LST)))
DEPEND_MAKE=$(addprefix ./, $(DEPEND_LST))
DEPEND_LST=libft

SRCS = $(addprefix ./src/, $(addsuffix .c, $(SRC_LIST)))
SRC_LIST = main\
		play\
		ft_init_board\
		ft_init_player\
		aff_player\
		aff_board\
		check_win\
		ft_read_input\
		ft_read_board\
		error

all : $(LIBS_DIR) $(NAME)

$(LIBS_DIR):
	echo "[ \033[0;33;mcreating lib folder\033[0m ]"
	mkdir $(LIBS_DIR)
	echo "[ \033[0;32;msuccessfuly\033[0m ]" $(LIBS_DIR) "was created"

%.a:
	@make -C $(DEPEND_MAKE)

$(NAME):
	echo "[ \033[0;33;mmake depend\033[0m ]"
	make $(DEPEND)
	echo "[ \033[0;33;mcompiling\033[0m ]"
	$(CC) $(FLAGS) $(DEPEND) $(INCS) $(SRCS) -o $(NAME)
	echo "[ \033[0;32;msuccessfuly\033[0m ]" $(NAME) "was created"

clean:
	echo "[ \033[0;33;mcleaning\033[0m ]"
	make clean -C./libft/
	rm -rf lib/

fclean: clean
	make fclean -C./libft/ && rm -f $(NAME)
	echo "[ \033[0;31;mdel\033[0m ]"$(NAME)

re: fclean all

.SILENT: all $(NAME) $(LIBS_DIR) clean fclean re

.PHONY: all clean fclean re
