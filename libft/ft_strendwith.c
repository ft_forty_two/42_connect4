/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strendwith.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fkalb <fkalb@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/29 00:59:11 by fkalb             #+#    #+#             */
/*   Updated: 2014/01/29 03:03:58 by fkalb            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

int			ft_strendwith(const char *s1, const char *s2)
{
	size_t	s1len;
	size_t	s2len;

	if ((s2len = ft_strlen(s2)) > (s1len = ft_strlen(s1)))
		return (0);
	return (ft_strequ(s1 + (s1len - s2len), s2));
}
