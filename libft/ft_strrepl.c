/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrepl.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fkalb <fkalb@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/01 11:37:48 by fkalb             #+#    #+#             */
/*   Updated: 2014/03/09 22:35:13 by kgilonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "stdlib.h"

char		*ft_strrepl(char *src, char *del, char *repl)
{
	char	*tmp;
	char	*str;

	repl = repl == NULL ? "" : repl;
	if (!src || !del)
		return (NULL);
	tmp = ft_strstr(src, del);
	if (!tmp)
		return (NULL);
	str = ft_strsub(src, 0, tmp - src);
	str = ft_strjoin_free(str, repl);
	str = ft_strjoin_free(str, tmp + ft_strlen(del));
	return (str);
}

char		*ft_strrepl_all(char *src, char *del, char *repl)
{
	int		i;
	int		len;
	char	*str;
	char	*tmp;

	i = -1;
	repl = repl == NULL ? "" : repl;
	tmp = ft_strrepl(src, del, repl);
	str = ft_strnew(1);
	len = ft_strlen(del);
	while (src[++i])
	{
		if (ft_strnequ(&src[i], del, len))
		{
			free(str);
			str = ft_strrepl(tmp, del, repl);
			free(tmp);
			tmp = ft_strdup(str);
		}
	}
	free(tmp);
	if (ft_strequ(str, ""))
		str = src;
	return (str);
}
