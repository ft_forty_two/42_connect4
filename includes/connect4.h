/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   connect4.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fkalb <fkalb@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 08:31:54 by fkalb             #+#    #+#             */
/*   Updated: 2014/03/10 19:52:49 by fkalb            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONNECT4_H
# define CONNECT4_H
# define BUFF_SIZE 1024
# define PGNAME "\033[;31;mconnect4\033[0m"
# define TRUE 1
# define FALSE 0
# define TOKENS 21
# define P1_IS_AI game->p1.is_ai
# define P2_IS_AI game->p2.is_ai
# define CASE_NC "\033[;;44m \033[0m"
# define RED "\033[;31;m@\033[0m"
# define CASE_RED "\033[;31;44m@\033[0m"
# define YELLOW "\033[;33;m@\033[0m"
# define CASE_YELLOW "\033[;33;44m@\033[0m"
# define USAGE "\033[;33;mUsage:\033[0m ./connect4 <height> <width>"

typedef struct	s_player
{
	int			is_ai;
	int			color;
	int			tokens;
}				t_player;

typedef struct	s_board
{
	int			height;
	int			width;
	char		**tab;
}				t_board;

typedef struct	s_game
{
	int			actu;
	int			numb_ai;
	int			connect_nb;
	int			end;
	t_board		board;
	t_player	p1;
	t_player	p2;
}				t_game;

int				ft_init_board(t_game *game, char **av);
int				ft_init_player(t_game *game);
int				aff_player(t_game *game, t_player *player, char *str, int play);
int				aff_board(t_board board);
int				ft_game_loop(t_game *game);
void			ft_play_ai(t_game *game, t_player *player);
void			ft_play_player(t_game *game, t_player *player);
int				ft_read_input(t_game game);
int				ft_read_board(t_game *game, t_player player);
int				ft_error1(char *error, int usage);
int				ft_error2(char *error, char *name, int usage);
int				ft_error3(char *prog, char *error, char *name, int usage);
void			ft_check_equ(t_game *game);
void			ft_check_win(t_game *game, int pl, int x, int y);
int				ft_check_line_and_col(t_game *game, int player, int x, int y);
int				ft_check_diag(t_game *game, int player, int x, int y);

#endif /* !CONNECT4_H */
