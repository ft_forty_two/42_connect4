/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aff_player.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kgilonne <kgilonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 16:49:24 by kgilonne          #+#    #+#             */
/*   Updated: 2014/03/09 23:44:45 by fkalb            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "connect4.h"
#include "libft.h"

int		aff_player(t_game *game, t_player *player, char *str, int play)
{
	if (str)
	{
		ft_putstr(str);
		if (player->color == 1)
			ft_putstr(" "YELLOW);
		else
			ft_putstr(" "RED);
		if (player->is_ai)
			ft_putstr(" ->> A.I.");
		ft_putchar('\n');
	}
	if (play && player->is_ai)
		ft_play_ai(game, player);
	else if (play && !(player->is_ai))
		ft_play_player(game, player);
	return (player->color);
}
