/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read_board.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kgilonne <kgilonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 23:08:58 by kgilonne          #+#    #+#             */
/*   Updated: 2014/03/09 23:41:03 by kgilonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "connect4.h"

static int		ft_read_line(t_game *game, t_player player)
{
	int		x;
	int		y;

	y = 0;
	while (game->board.tab[y])
	{
		x = 0;
		while (game->board.tab[y][x + 2])
		{
			if (game->board.tab[y][x] != ' ' && game->board.tab[y][x + 1] != ' '
				&& game->board.tab[y][x + 2] == ' ')
			{
				game->board.tab[y][x + 2] = player.color;
				ft_check_win(game, player.color, x + 2, y);
				return (1);
			}
			x++;
		}
		y++;
	}
	return (0);
}

static int		ft_read_col(t_game *game, t_player player)
{
	int		x;
	int		y;

	y = 0;
	while (game->board.tab[y + 2])
	{
		x = 0;
		while (game->board.tab[y][x])
		{
			if (game->board.tab[y][x] != ' ' && game->board.tab[y + 1][x] != ' '
				&& game->board.tab[y + 2][x] == ' ')
			{
				game->board.tab[y + 2][x] = player.color;
				ft_check_win(game, player.color, x , y + 2);
				return (1);
			}
			x++;
		}
		y++;
	}
	return (0);
}


int		ft_read_board(t_game *game, t_player player)
{
	if (ft_read_line(game, player))
		return (1);
	if (ft_read_col(game, player))
		return (1);
	return (0);
}
