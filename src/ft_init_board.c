/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_board.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kgilonne <kgilonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 17:10:15 by kgilonne          #+#    #+#             */
/*   Updated: 2014/03/09 22:38:59 by kgilonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "connect4.h"
#include "libft.h"

int		ft_malloc_board(t_board *board)
{
	int		i;

	if (!(board->tab = (char**)malloc(sizeof(char*) * (board->height + 1))))
		return (ft_error2("malloc", "Out of memory", FALSE));
	i = 0;
	while (i < board->height)
	{
		board->tab[i] = (char*)malloc(sizeof(char) * (board->width + 1));
		ft_memset(board->tab[i], ' ', board->width);
		board->tab[i++][board->width] = '\0';
	}
	board->tab[i] = NULL;
	return (0);
}

int		ft_init_board(t_game *game, char **av)
{
	if ((game->board.height = ft_atoi(av[1])) < 6
		|| (game->board.width = ft_atoi(av[2])) < 7)
		return (ft_error2("invalid size", "require 6x7 or more", FALSE));
	if (ft_malloc_board(&game->board))
		return (1);
	return (0);
}
