/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read_input.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kgilonne <kgilonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 20:45:21 by kgilonne          #+#    #+#             */
/*   Updated: 2014/03/10 19:14:37 by kgilonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "connect4.h"
#include "libft.h"

static int		ft_check_input(t_game game, char *input)
{
	int		i;
	int		column_nb;
	int		flag;
	char	*tmp;

	i = 0;
	flag = 0;
	tmp = ft_strtrim(input);
	while (tmp[i] != '\0')
	{
		if (tmp[i] < 48 || tmp[i] > 57)
			flag += 1;
		i++;
	}
	column_nb = ft_atoi(tmp);
	ft_strdel(&tmp);
	if (flag > 0 || game.board.tab[0][column_nb - 1] != ' ')
		return (1);
	if (column_nb == 0 || column_nb > game.board.width)
		return (1);
	else
		return (0);
}

int				ft_read_input(t_game game)
{
	int		nbread;
	char	*buff;
	int		column_nb;

	buff = ft_strnew(64);
	ft_putstr("Please enter a valid column number: ");
	if ((nbread = read(0, buff, 64)) == -1)
		return (ft_error2("read", "ERROR", FALSE));
	if (*buff == '\0')
	{
		ft_putendl("ABANDON");
		return (-1);
	}
	buff[nbread - 1] = '\0';
	if (ft_check_input(game, buff))
	{
		ft_putendl("Invalid column number.");
		ft_strdel(&buff);
		return (ft_read_input(game));
	}
	column_nb = ft_atoi(buff);
	ft_strdel(&buff);
	return (column_nb);
}
