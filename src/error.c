/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fkalb <fkalb@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 08:31:29 by fkalb             #+#    #+#             */
/*   Updated: 2014/03/08 14:07:31 by fkalb            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "connect4.h"

int		ft_error3(char *prog, char *error, char *name, int usage)
{
	prog = prog == NULL ? PGNAME : prog;
	ft_putstr_fd(prog, 2);
	ft_putstr_fd(": ", 2);
	ft_putstr_fd(error, 2);
	if (name)
	{
		ft_putstr_fd(": ", 2);
		ft_putstr_fd(name, 2);
	}
	ft_putstr_fd("\n", 2);
	if (usage)
		ft_putendl_fd(USAGE, 2);
	return (-1);
}

int		ft_error2(char *error, char *name, int usage)
{
	return (ft_error3(NULL, error, name, usage));
}

int		ft_error1(char *error, int usage)
{
	return (ft_error3(NULL, error, NULL, usage));
}

