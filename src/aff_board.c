/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aff_board.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kgilonne <kgilonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 17:43:04 by kgilonne          #+#    #+#             */
/*   Updated: 2014/03/09 19:46:24 by kgilonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "connect4.h"
#include "libft.h"

static void		aff_token(int player)
{
	if (player == ' ')
		ft_putchar(' ');
	else if (player == 1)
		ft_putstr(YELLOW);
	else
		ft_putstr(RED);
}

static void		aff_separation(t_board board)
{
	int		i;

	i = -1;
	while (i < (board.width * 2))
	{
		ft_putstr("\033[34m-\033[0m");
		i++;
	}
	ft_putchar('\n');
}

static void		aff_col_nb(t_board board)
{
	int		i;

	i = 1;
	while (i < (board.width + 1))
	{
		ft_putchar(' ');
		ft_putnbr(i % 10);
		i++;
	}
	ft_putchar('\n');
}

int				aff_board(t_board board)
{
	int		i;
	int		j;

	i = 0;
	ft_putchar('\n');
	while (board.tab[i])
	{
		j = 0;
		ft_putstr("\033[34m|\033[0m");
		while (board.tab[i][j])
		{
			aff_token(board.tab[i][j]);
			ft_putstr("\033[34m|\033[0m");
			j++;
		}
		ft_putchar('\n');
		i++;
	}
	aff_separation(board);
	aff_col_nb(board);
	return (0);
}
