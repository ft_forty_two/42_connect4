/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   play.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fkalb <fkalb@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 20:24:52 by fkalb             #+#    #+#             */
/*   Updated: 2014/03/10 19:21:51 by kgilonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "connect4.h"
#include "libft.h"

int			ft_game_loop(t_game *game)
{
	while (game->end == FALSE)
	{
		ft_check_equ(game);
		if (game->end == TRUE)
			break ;
		aff_player(game, &(game->p1), "Player 1 turn:", TRUE);
		aff_board(game->board);
		if (game->end == TRUE)
			break ;
		aff_player(game, &(game->p2), "Player 2 turn:", TRUE);
		aff_board(game->board);
	}
	return (0);
}

void		ft_play_ai(t_game *game, t_player *player)
{
	int		y;

	y = game->board.height;
	if (player->tokens == 0)
	{
		ft_putendl("AI has no token, end of the game.");
		game->end = TRUE;
		return ;
	}
	if (ft_read_board(game, *player))
		return ;
	while (--y >= 0)
	{
		if (game->board.tab[y][game->board.width / 2] == ' ')
		{
			game->board.tab[y][game->board.width / 2] = player->color;
			ft_check_win(game, player->color, game->board.width / 2, y);
			break ;
		}
	}
	if (player->tokens > 0)
		(player->tokens)--;
}

void		ft_play_player(t_game *game, t_player *player)
{
	int		x;
	int		y;

	if (player->tokens == 0)
	{
		ft_putendl("You have no token, end of the game.");
		game->end = TRUE;
		return ;
	}
	if ((x = (ft_read_input(*game)) - 1) == -2)
		game->end = TRUE;
	y = game->board.height;
	while (--y >= 0)
	{
		if (game->board.tab[y][x] == ' ')
		{
			game->board.tab[y][x] = (*player).color;
			if (player->tokens > 0)
				(player->tokens)--;
			ft_check_win(game, (*player).color, x, y);
			break ;
		}
	}
}
