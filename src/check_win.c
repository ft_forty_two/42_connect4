/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_win.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fkalb <fkalb@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 14:19:41 by fkalb             #+#    #+#             */
/*   Updated: 2014/03/09 22:55:03 by fkalb            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "connect4.h"

#define MAX_X (game->board.width)
#define MAX_Y (game->board.height)
#define RIGHT (game->board.tab[y][x + i])
#define LEFT (game->board.tab[y][x - i])
#define DOWN (game->board.tab[y + i][x])
#define UP (game->board.tab[y - i][x])
#define DIAG_DR (game->board.tab[y + i][x + i])
#define DIAG_UL (game->board.tab[y - i][x - i])
#define DIAG_DL (game->board.tab[y + i][x - i])
#define DIAG_UR (game->board.tab[y - i][x + i])

void		ft_check_equ(t_game *game)
{
	int		i;

	i = 0;
	while (game->board.tab[0][i] && game->board.tab[0][i] != ' ')
		i++;
	if (!game->board.tab[0][i])
	{
		ft_putendl("->>\033[;35;m Match Nul\033[0m");
		game->end = TRUE;
	}
}

int			ft_check_diag(t_game *game, int player, int x, int y)
{
	int		i;
	int		count1;
	int		count2;

	i = 0;
	count1 = game->board.tab[y][x] == player ? 1 : 0;
	count2 = count1;
	while (++i && (y + i) < MAX_Y && (x + i) < MAX_X && DIAG_DR == player)
		count1++;
	i = 0;
	while (++i && (y - i) >= 0 && (x - i) >= 0 && DIAG_UL == player)
		count1++;
	i = 0;
	while (++i && (y + i) < MAX_Y && (x - i) >= 0 && DIAG_DL == player)
		count2++;
	i = 0;
	while (++i && (y - i) >= 0 && (x + i) < MAX_X && DIAG_UR == player)
		count2++;
	if (count1 >= game->connect_nb || count2 >= game->connect_nb)
		return (1);
	return (0);
}

int			ft_check_line_and_col(t_game *game, int player, int x, int y)
{
	int		i;
	int		count1;
	int		count2;

	i = 0;
	count1 = game->board.tab[y][x] == player ? 1 : 0;
	count2 = count1;
	while (++i && (x + i) < MAX_X && RIGHT == player)
		count1++;
	i = 0;
	while (++i && (x - i) >= 0 && LEFT == player)
		count1++;
	i = 0;
	while (++i && (y - i) >= 0 && UP == player)
		count2++;
	i = 0;
	while (++i && (y + i) < MAX_Y && DOWN == player)
		count2++;
	if (count1 >= game->connect_nb || count2 >= game->connect_nb)
		return (1);
	return (0);
}

void		ft_check_win(t_game *game, int pl, int x, int y)
{
	if (!game || !game->board.tab || x >= MAX_X || y >= MAX_Y)
		return ;
	if (ft_check_line_and_col(game, pl, x, y) || ft_check_diag(game, pl, x, y))
	{
		if (pl == 1)
			ft_putstr("->> "YELLOW" Win");
		else
			ft_putstr("->> "RED" Win");
		game->end = TRUE;
	}
}

