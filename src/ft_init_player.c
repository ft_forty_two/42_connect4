/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_player.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kgilonne <kgilonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 16:44:48 by kgilonne          #+#    #+#             */
/*   Updated: 2014/03/09 23:44:40 by fkalb            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <time.h>
#include <stdlib.h>
#include "connect4.h"

static void		ft_get_player_color(t_game *game)
{
	srand(time(NULL));
	game->p1.color = (rand() % 2) + 1;
	game->p2.color = (((game->p1.color - 1) == 0) ? 2 : 1);
}

static void		ft_get_player_nb(t_game *game)
{
	P1_IS_AI = ((((rand() % 3) == 1) && (game->numb_ai > 0)) ? TRUE : FALSE);
	P1_IS_AI = ((game->numb_ai == 2) ? TRUE : P1_IS_AI);
	game->numb_ai = ((P1_IS_AI) ? (game->numb_ai - 1) : game->numb_ai);
	P2_IS_AI = ((game->numb_ai > 0) ? TRUE : FALSE);
}

int				ft_init_player(t_game *game)
{
	game->actu = 1;
	game->p1.tokens = TOKENS;
	game->p2.tokens = TOKENS;
	ft_get_player_color(game);
	ft_get_player_nb(game);
	aff_player(game, &(game->p1), "Player 1 are", FALSE);
	aff_player(game, &(game->p2), "Player 2 are", FALSE);
	return (0);
}
