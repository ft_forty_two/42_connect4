/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fkalb <fkalb@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 08:31:00 by fkalb             #+#    #+#             */
/*   Updated: 2014/03/15 15:34:50 by fkalb            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "connect4.h"
#include "libft.h"
#include <stdlib.h>

static int		init_nfo(t_game *game)
{
	game->p1.tokens = TOKENS;
	game->p2.tokens = TOKENS;
	game->end = FALSE;
	game->numb_ai = 0;
	game->connect_nb = 4;
	return (0);
}

int				main(int ac, char **av)
{
	t_game		game;

	if (ac < 3)
		return (ft_error1("Too few arguments", TRUE));
	if (ac > 3)
		return (ft_error1("Too many arguments", TRUE));
	if (ft_init_board(&game, av))
		return (1);
	if (init_nfo(&game) == -1)
		return (1);
	ft_init_player(&game);
	aff_board(game.board);
	return (ft_game_loop(&game));
}

